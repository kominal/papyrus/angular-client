import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, InjectionToken, Injector, ModuleWithProviders, NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
	MissingTranslationHandler,
	MissingTranslationHandlerParams,
	TranslateDirective,
	TranslateLoader,
	TranslateModule,
	TranslateParser,
	TranslatePipe,
	TranslateService,
} from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { PapyrusPaginatorIntl } from './classes/papyrusPaginatorIntl';
import { LANGUAGES } from './components/language-selector/language-selector.component';

export const BASE_URL = new InjectionToken<string | undefined>('PAPYRUS_BASE_URL');
export const TENANT_ID = new InjectionToken<string>('PAPYRUS_TENANT_ID');
export const PROJECT_NAME = new InjectionToken<string>('PAPYRUS_PROJECT_ID');
export const DEFAULT_LANGUAGE = new InjectionToken<string[] | undefined>('PAPYRUS_DEFAULT_LANGUAGE');
export const USE_BROWSER_LANGUAGE = new InjectionToken<string[] | undefined>('PAPYRUS_USE_BROWSER_LANGUAGE');

export type MissingTranslation = {
	language: string;
	key: string;
};

export class PapyrusMissingTranslationHandler implements MissingTranslationHandler {
	triggerSubject = new Subject<void>();
	missingTranslations: MissingTranslation[] = [];
	forwardedMissingTranslations: MissingTranslation[] = [];

	constructor(httpClient: HttpClient, injector: Injector) {
		this.triggerSubject.pipe(debounceTime(60000)).subscribe(async () => {
			try {
				const forwarding = [...this.missingTranslations];
				this.forwardedMissingTranslations.push(...forwarding);
				this.missingTranslations = [];

				const baseUrl = injector.get(BASE_URL) || 'https://papyrus.kominal.app';
				const tenantId = injector.get(TENANT_ID);
				const projectName = injector.get(PROJECT_NAME);
				await httpClient
					.post(`${baseUrl}/controller/tenants/${tenantId}/translations/projects/${projectName}/missing`, forwarding)
					.toPromise();
			} catch (e) {
				console.log(e);
			}
		});
	}

	handle(params: MissingTranslationHandlerParams) {
		const { key } = params;
		const language = params.translateService.currentLang || params.translateService.defaultLang || 'en';
		if (
			!this.missingTranslations.find((m) => m.language === language && m.key === key) &&
			!this.forwardedMissingTranslations.find((m) => m.language === language && m.key === key)
		) {
			this.missingTranslations.push({ language, key });
			this.triggerSubject.next();
		}
	}
}

export class PapyrusHttpLoader implements TranslateLoader {
	constructor(private httpClient: HttpClient, private injector: Injector) {}
	public getTranslation(lang: string): Observable<Object> {
		const baseUrl = this.injector.get(BASE_URL) || 'https://papyrus.kominal.app';
		const tenantId = this.injector.get(TENANT_ID);
		const projectName = this.injector.get(PROJECT_NAME);
		return this.httpClient.get(`${baseUrl}/controller/tenants/${tenantId}/translations/projects/${projectName}/languages/${lang}`);
	}
}

export function PapyrusInitFactory(
	translateService: TranslateService,
	languages: string[] | undefined,
	defaultLanguage: string | undefined,
	useBrowserLanguage: boolean | undefined
) {
	const defaultPapyrusLanguage = defaultLanguage || 'en';

	const selectableLanguages = languages || ['en'];
	translateService.setDefaultLang(defaultPapyrusLanguage);

	const language = localStorage.getItem('language');
	const browserLang = translateService.getBrowserLang();

	if (language && selectableLanguages.includes(language)) {
		translateService.use(language);
	} else if (useBrowserLanguage === true && browserLang && selectableLanguages.includes(browserLang)) {
		translateService.use(browserLang);
	} else {
		translateService.use(defaultPapyrusLanguage);
	}

	return () => {};
}

export const translateModuleRoot = TranslateModule.forRoot({
	loader: {
		provide: TranslateLoader,
		useClass: PapyrusHttpLoader,
		deps: [HttpClient, Injector],
	},
	missingTranslationHandler: {
		provide: MissingTranslationHandler,
		useClass: PapyrusMissingTranslationHandler,
		deps: [HttpClient, Injector],
	},
});

@NgModule({
	declarations: [],
	imports: [CommonModule, MatMenuModule, MatButtonModule, MatTooltipModule, HttpClientModule, translateModuleRoot, MatIconModule],
	exports: [
		MatMenuModule,
		MatButtonModule,
		MatTooltipModule,
		MatIconModule,
		HttpClientModule,
		TranslateModule,
		TranslatePipe,
		TranslateDirective,
	],
	providers: [
		{
			provide: APP_INITIALIZER,
			useFactory: PapyrusInitFactory,
			deps: [TranslateService, LANGUAGES, DEFAULT_LANGUAGE, USE_BROWSER_LANGUAGE],
			multi: true,
		},
		{
			provide: MatPaginatorIntl,
			useClass: PapyrusPaginatorIntl,
			deps: [TranslateService, TranslateParser],
		},
	],
})
export class PapyrusClientModule {
	static forRoot(config: {
		baseUrl?: string;
		tenantId: string;
		projectName: string;
		languages?: string[];
		defaultLanguage?: string;
		useBrowserLanguage?: string;
	}): ModuleWithProviders<PapyrusClientModule> {
		return {
			ngModule: PapyrusClientModule,
			providers: [
				{ provide: BASE_URL, useValue: config.baseUrl },
				{ provide: TENANT_ID, useValue: config.tenantId },
				{ provide: PROJECT_NAME, useValue: config.projectName },
				{ provide: LANGUAGES, useValue: config.languages },
				{ provide: DEFAULT_LANGUAGE, useValue: config.defaultLanguage },
				{ provide: USE_BROWSER_LANGUAGE, useValue: config.useBrowserLanguage },
			],
		};
	}
}
